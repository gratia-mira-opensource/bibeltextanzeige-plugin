# Bibeltextanzeige-Plugin
Durchsucht einen Text und verlinkt die Bibelstellen und Bibelstellabfolgen mit der Schlachter-2000 Bibelübersetzung.
Hier finden Sie eine [Musterseite](https://www.gratia-mira.ch/j3/gedankenanstoesse/alle-beitraege/nach-bibelbuch/1-mose/165-1-mose-2/1158-1-mose-2-18) des Plugins.

# Einflussnahme
Damit die Stellen mit Sicherheit erkannt werden, sollten sie schön geschrieben werden, wobei der Anfangsbuchstabe gross sein muss. Fügt man ~-Zeichen vor eine Bibelstelle die Bibelstelle nicht erkannt, das Zeichen selbst aber bei der Ausgabe herausgefiltert.

# Systembedinungen | Technisches
Unter Apache/2.4.48 (Win64) OpenSSL/1.1.1k PHP/7.4.20 (loalhost), funktioniert das Plugin nicht ganz optimal. 
Unter Linux OS / PHP 8.0.11 treten diese Fehler nicht auf.

Das Plugin durchsucht den Text, bevor er angezeigt wird. Somit wird die Datenbank nicht verändert.

# Datenschutz
Mustertext: Wer die unterstrichenen Bibelstellenangaben auf dieser Webseite anklickt, wird dadurch mit der Webseite https://www.gratia-mira.ch verbunden und bekommt von dort den entsprechenden Bibelvers in einem eigenen Fenster angezeigt. Dabei werden Daten an diese Webseite übermittelt und dort entsprechend der Datenschutzinformationen verarbeitet. Siehe die Datenschutzinformation von Gratia Mira: https://www.gratia-mira.ch/j3/gratia-mira-ch-mehr/startseite/719-datenschutzerklaerung.
