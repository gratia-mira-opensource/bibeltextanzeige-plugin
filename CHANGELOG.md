 # 1.1.0 ??.??.????
- [ ] Wordpress aktualisieren
- [x] Joomla aktualisieren
 * Verbesserung: Code komplett überarbeitet.
 * Verbesserung: Bessere Erkennung, sauberere Ausgabe der Bibelstellen und Kapitel Links.
 * Sicherheit: `rel="noopener"` bei "_blank"
 * Fehler: bei Erkennung von Philemon bei gewissen Anwendungen
 # 1.0.7 06.12.2021
 * Kleine Verbesserungen in der Erkennung.
 * Erste Version, die öffentlich angeboten wird - so Gott will!
 * Verschiedene Plugins vereinheitlicht.
 # 1.0.5 21.10.2021
 * Verbesserung: Definition Bibelstele, Kapitel und VersKapitel verbessert
 * Verbesserung: Oft falsch als Bibelstelle erkannte Muster (`Am 13.02.; Am 1. Juni; 15:03 Uhr; 1000+; Ob 1,?22+`) werden vor der Erkennung geschützt.
 * Verbesserung: `non-bracking-space` Zeichen führt zu keinem Fehler mehr
 * Joomla: Update-Server hinzugefügt
