<?php
/*
Plugin Name: Automatische Bibeltextanzeige (Schlachter2000)
Plugin URI: https://gitlab.com/gratia-mira-opensource/bibeltextanzeige-plugin
Description: Findet Bibelstellen und verlinkt diese automatisch. 
Author: Christoph von gratia-mira.ch
Author URI: https://gratia-mira.ch
Version: 1.0.7
*/

// Direkten Aufruf verhindern
if (!defined('WPINC')) {
  die;
}

// Die Funktion bibelstellen_gratia_mira_ch zum Filter the_content hinzufügen
add_filter( 'the_content', 'bibelstellen_gratia_mira_ch' );

function bibelstellen_gratia_mira_ch($content) {
	// Je nach CMS Unterschiedlich
	$Inhalt = $content;
	
	// Bei Änderungen, bzw. Erweiterungen auch tool.php (mod_bibelverse) ändern
	$Bibelbuecher = preg_replace('/\|\s{0,}\n\s{0,}\|/','|',
		'([1-5]\.?[\s ]?Buch Mose|[1-5]\.?[\s ]?Mose|[1-5]\.?[\s ]?Mos|[1-5]\.?[\s ]?Mo|[1-5]\.?[\s ]?M|Genesis|Gen|Exodus|Exod|Ex|Levitikus|Lev|Numeri|Num|Deuteronomium|Deut|Dtn|
		|Josua|Jos|
		|Richter|Ri|
		|Ruth|Rut|Rt|Ru|
		|[1-2]\.?[\s ]?Buch Samuel|[1-2]\.?[\s ]?Samuel|[1-2]\.?[\s ]?Sam|[1-2]\.?[\s ]?S|
		|[1-2]\.?[\s ]?Buch Könige|[1-2]\.?[\s ]?Könige|[1-2]\.?[\s ]?Kön|[1-2]\.?[\s ]?Kg|[1-2]\.?[\s ]?Kö|
		|[1-2]\.?[\s ]?Chronika?|[1-2]\.?[\s ]?Chronik|[1-2]\.?[\s ]?Chr|[1-2]\.?[\s ]?Ch|
		|Esra|Esr|
		|Nehemia|Neh|Ne|
		|Esther|Est|
		|Hiob|Hi|Ijob|
		|Psalm|Ps|Psalmen|
		|Sprüche|Spr|Sprichwörter|
		|Prediger|Pred|Prd|Kohelet|Koh|
		|Hohelied|Hohel|Hld|Hoh|Hl|
		|Jesaja|Jes|
		|Jeremia|Jer|
		|Klagelied|Klagel|Klgl|Klag|Kla|Klg|
		|Hesekiel|Hes|Ez|
		|Daniel|Dan|Da|
		|Hosea|Hos|
		|Joel|Joe|
		|Amos|Am|
		|Obadja|Obad|Ob|
		|Jona|Jon|
		|Micha|Mich|Mi|
		|Nahum|Nah|
		|Habakuk|Hab|
		|Zephanja|Zefania|Zeph|Zef|Zep|
		|Haggai|Hag|Hg|
		|Sacharja|Sach|Sac|
		|Maleachi|Mal|
		|Matthäusevangelium|Matthäus|Matth|Mat|Mth|Mt|
		|Markusevangelium|Markus|Mark|Mk|
		|Lukasevangelium|Lukas|Luk|Lk|
		|Apostelgeschichte|Apg|Ag|Ap|
		|Römerbrief|Römer|Röm|Rm|Rö|
		|[1-2]\.?[\s ]?Korintherbrief|[1-2]\.?[\s ]?Korinther|[1-2]\.?[\s ]?Kor|[1-2]\.?[\s ]?Kr|
		|Galaterbrief|Galater|Gal|Ga|
		|Epheserbrief|Epheser|Eph|
		|Philemonbrief|Philemon|Phlm|Phm|
		|Philipperbrief|Philipper|Phil|Php|
		|Kolosserbrief|Kolosser|Kol|
		|[1-2]\.?[\s ]?Thessalonicherbrief|[1-2]\.?[\s ]?Thessalonicher|[1-2]\.?[\s ]?Thess|[1-2]\.?[\s ]?Thes|[1-2]\.?[\s ]?Th|
		|[1-2]\.?[\s ]?Timotheusbrief|[1-2]\.?[\s ]?Timotheus|[1-2]\.?[\s ]?Tim|[1-2]\.?[\s ]?Tm|
		|Titusbrief|Titus|Tit|Tt|
		|Hebräerbrief|Hebräer|Heber|Hebr|Heb|
		|Jakobusbrief|Jakobus|Jak|Jk|
		|[1-2]\.?[\s ]?Petrusbrief|[1-2]\.?[\s ]?Petrus|[1-2]\.?[\s ]?Petr|[1-2]\.?[\s ]?Pet|[1-2]\.?[\s ]?Ptr|[1-2]\.?[\s ]?Pe|[1-2]\.?[\s ]?Pt|[1-2]\.?[\s ]?P|
		|[1-3]\.?[\s ]?Johannesbrief|[1-3]\.?[\s ]?Johannes|[1-3]\.?[\s ]?Joh|[1-3]\.?[\s ]?Jh|[1-3]\.?[\s ]?Jo|
		|Johannesevangelium|Johannes|Joh|Jh|
		|Judasbrief|Judas|Jud|
		|Offenbarung|Offb|Off)');
		
	if(!defined('BibelstelleREGEXP')) { define('BibelstelleREGEXP',$Bibelbuecher . '[.,]?[\s ]+[1-9]\d{0,2}[b-d]?[\s ]?[,.:][\s ]?[1-9]\d{0,2}[a-d]?[\s ]?f?f?[\s ]?-?[\s ]?\d{0,3}[a-d]?[\s ]?[,.:]?[\s ]?\d{0,3}[a-d]?'); }
	if(!defined('KapitelREGEXP')) { define('KapitelREGEXP',$Bibelbuecher . '[.,]?[\s ]+[1-9]\d{0,2}[\s ]?-?[\s ]?\d{0,3}'); }
	if(!defined('KapitelVersREGEXP')) { define('KapitelVersREGEXP','[\s ]?[1-9]\d{0,2}[b-d]?[\s ]?[,.:-]?[\s ]?\d{0,3}[a-d]?[\s ]?f?f?[\s ]?-?[\s ]?\d{0,3}[a-d]?[\s ]?[,.:]?[\s ]?\d{0,3}[a-d]?[\s ]?'); }
 
	// Links und Überschriften schützen
	if(!function_exists('SchutzmaskeOhneStelle')) {
		function SchutzmaskeOhneStelle($a){
			return '??-- ' . preg_replace('/(\d)/','&$1',base64_encode($a[0])) . ' --$$'; 
		}
	}
	// Bereits bearbeitete Stellenabfolge schützen
	if(!function_exists('SchutzmaskeStellenabfolge')) {
		function SchutzmaskeStellenabfolge($b){  
			return $b[1] . '??-- ' . preg_replace('/(\d)/','&$1',base64_encode('<a target="popup" onclick="window.open(`about:blank`, `popup`, `scrollbars=yes, toolbar=no,status=no,resizable=yes,menubar=no,location=no,directories=no,top=10,left=3000,width=600,height=800`)" href="https://www.gratia-mira.ch/j3/?Stichwort=' . urlencode($b[2]) . '&Plugin=Stellenabfolge" title="Bibelstellen aufschlagen" style="color:currentcolor;text-decoration:underline;" target="_blank" rel="noopener">' . $b[2] . '</a>')) . ' --$$' . $b[4]; 
		}
	}
	// Bereits bearbeitete Stellen schützen
	if(!function_exists('SchutzmaskeStellen')) {
		function SchutzmaskeStellen($c){
			$Link = '<a target="popup" onclick="window.open(`about:blank`, `popup`, `scrollbars=yes, toolbar=no,status=no,resizable=yes,menubar=no,location=no,directories=no,top=10,left=3000,width=600,height=800`)" href="https://www.gratia-mira.ch/j3/?Stichwort=' . urlencode($c[2]) . '&Plugin=Bibelstelle" title="Bibelstelle aufschlagen" style="color:currentcolor;text-decoration:underline;" target="_blank" rel="noopener">' . $c[2] . '</a>';
			return $c[1] . '??-- ' . preg_replace('/(\d)/','&$1',base64_encode($Link)) . ' --$$' . $c[4]; 
		}
	}
	
		// Überschriften und oft falsch als Bibelstelle erkannte Muster schützen
		$SuchenLinksUeberschriften = array(
			'/<form.*\/form>/Us','/<a.*\/a>/Us', // Formulardaten
			'/(<h[1-9]>.*\/h[1-9]>)/Us', // Überschriften
			'/[0-2]?\d[:.][0-5]\d[\s ]?Uhr/', // 15:03 Uhr;
			'/\d{4,10}/', // 1000+
			'/(\b[^Ps0-9]{2}|[^\s (alm0-9]{3}|[^\s (men0-9]{3})[\s ]\d{3}/', // Lk 707 (alles dreistellige ausser Psalmen)
			'/Am[\s ]?\d{1,2}\.\d{1,2}\./', // Am 13.02.
			'/Am[\s ]?\d{1,2}\.[\s ]?(Jan|Feb|März|Apr|Mai|Jun|Jul|Aug|Sept|Okt|Nov|Dez)/', // Am 1. Juni
			'/Amo?s?[\s ]?([1-9]\d[.:,]?|\d{3})/', // Am 10+
			'/Oba?d?j?a?[\s ]?(1?,?2[2-9]|1?[.:,]?[3-9][0-9]|1?[.:,]?\d{3})/', // Ob 1,?22+
			'/Dan?i?e?l?[\s ]?(1[3-9][.:,]?|[2-9]\d,?|\d{3})/', // Da 13,?+
			'/\d{1,2}[\s ]+[A-ZÜÖÄ][a-zäöüß]{2,20}(([!?.;:]?[\s ]?|[\s ]+)[<]|([!?.;:]?[\s ]?|[\s ]+)$|[\s ]+[a-zäöüß])/m'); // 13 Esel</p> / 15 Kühe / 16 Menschen und 
		$Maskiert = preg_replace_callback($SuchenLinksUeberschriften, 'SchutzmaskeOhneStelle', $Inhalt);
		
		// Bibelstellenabfolge und Kapitelangaben in einen Link umwandeln + maskieren
		$SuchenBibelstellenabfolge = array(
			'/([(][^)]{0,20}[^~]?)(' . constant('BibelstelleREGEXP') . '[a-d]?[\s ]?[;.+].*)([)])/U', // (Lk 2,2; Mk 1,1; Joh 3,3-4)
			'/([(][^)]{0,20}[^~]?)(' . $Bibelbuecher . constant('KapitelVersREGEXP') . '[A-Za-zÄÖÜäöü.&;]{1,20}' . constant('KapitelVersREGEXP') . ')([)])/U', // (Lukas 2 und 3)
			'/([^~])(\b' . $Bibelbuecher . constant('KapitelVersREGEXP') . '[;]'. constant('KapitelVersREGEXP') . '[0-9\-\–?!\[\]\*+&\|\/=;.:,abcx  ]{0,200})([;.:?!<)]|,[\s ]|[^;][\s ][A-ZÄÖÜa-zäöü])/', // Lk 2,1;2,2;
			'/([^~])(\b' . $Bibelbuecher . constant('KapitelVersREGEXP') . '[A-Za-zÄÖÜäöü.&;]{1,20}[\s ])(' . $Bibelbuecher . constant('KapitelVersREGEXP') . ')/', // Römer 13 und 1. Petrus 2 (Workaround)
			'/([^~])(\b' . $Bibelbuecher . constant('KapitelVersREGEXP') . '[A-Za-zÄÖÜäöü.&;]{0,20}' . constant('KapitelVersREGEXP') . '[A-Za-zÄÖÜäöü.&;]{1,20}' . constant('KapitelVersREGEXP') . ')([;.:?!<)\s]| |,[\s ]|[^;][\s ][A-ZÄÖÜa-zäöü])/', // Lukas 12,1 und 13,5 (sowie 15,1)
			'/([^~])(' . constant('KapitelREGEXP') . '[A-Za-zÄÖÜäöü.&;]{1,20}' . constant('KapitelVersREGEXP') . ')([;,.:<)\s]| )/'); // Mt 1 und 2
		
		
		// Stellenabfolge
		$LinksFolgeMaskiert = preg_replace_callback($SuchenBibelstellenabfolge,'SchutzmaskeStellenabfolge',$Maskiert);
		
		// Bibelstelle in eine Link umwandeln + maskieren
		$SuchenStelle = array('/([^~])(' . constant('BibelstelleREGEXP') . ')([;,.:?!<)\s]| )/','/([^~])(' . constant('KapitelREGEXP') . ')([;,.:<)\s]| )/');
		$LinksStellenMaskiert = preg_replace_callback($SuchenStelle,'SchutzmaskeStellen',$LinksFolgeMaskiert);
		
		// Ersetzt am Ende alle ~, die dazu bestimmt sind, dass die Bibelstelle nicht verlinkt wird
		$AllesMaskiert = preg_replace('/\~(' . constant('KapitelREGEXP') . ')/','$1',$LinksStellenMaskiert);
	
	// Links und Überschriften wiederherstellen
	if(!function_exists('WiederherstellenMaske')) {
		function WiederherstellenMaske($d) {
			return 	base64_decode(str_replace(array('??-- ',' --$$','&'),'',$d[0])); 
		}
	}
	
	// Zurücksetzen bis alles gut ist :)
	$Fertig = $AllesMaskiert;
	do {
		$Fertig = preg_replace_callback('/\?\?\-\- .* \-\-\$\$/U','WiederherstellenMaske',$Fertig);
	} while(preg_match('/\?\?\-\- .* \-\-\$\$/',$Fertig) == true);
	
	// Je nach CMS anders
	return $Fertig;
}

?>
