<?php
/**
 * @version        1.0.7
 * @package        Joomla
 * @subpackage     Content
 * @copyright      Copyright (C) 2005 - 2008 Open Source Matters. All rights reserved.
 * @license        GNU/GPL, see LICENSE.php
 * Joomla! is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * See COPYRIGHT.php for copyright notices and details.
 * https://docs.joomla.org/J3.x:Creating_a_content_plugin
 */

// Kein direkter Zugriff erlauben
defined( '_JEXEC' ) or die( 'Restricted access' );


class plgContentBibeltextanzeige extends JPlugin {

	public static function onContentPrepare( $context, $article ) {

		// Je nach CMS Unterschiedlich
		$Inhalt = $article->text;

		// Bei Änderungen, bzw. Erweiterungen auch tool.php (mod_bibelverse) ändern
		$Bibelbuecher = preg_replace( '/\|\s{0,}\n\s{0,}\|/', '|',
			'([1-5]\.?[\s ]?Buch Mose|[1-5]\.?[\s ]?Mose|[1-5]\.?[\s ]?Mos|[1-5]\.?[\s ]?Mo|[1-5]\.?[\s ]?M|Genesis|Gen|Exodus|Exod|Ex|Levitikus|Lev|Numeri|Num|Deuteronomium|Deut|Dtn|
		|Josua|Jos|
		|Richter|Ri|
		|Ruth|Rut|Rt|Ru|
		|[1-2]\.?[\s ]?Buch Samuel|[1-2]\.?[\s ]?Samuel|[1-2]\.?[\s ]?Sam|[1-2]\.?[\s ]?S|
		|[1-2]\.?[\s ]?Buch Könige|[1-2]\.?[\s ]?Könige|[1-2]\.?[\s ]?Kön|[1-2]\.?[\s ]?Kg|[1-2]\.?[\s ]?Kö|
		|[1-2]\.?[\s ]?Chronika?|[1-2]\.?[\s ]?Chronik|[1-2]\.?[\s ]?Chr|[1-2]\.?[\s ]?Ch|
		|Esra|Esr|
		|Nehemia|Neh|Ne|
		|Esther|Est|
		|Hiob|Hi|Ijob|
		|Psalm|Ps|Psalmen|
		|Sprüche|Spr|Sprichwörter|
		|Prediger|Pred|Prd|Kohelet|Koh|
		|Hohelied|Hohel|Hld|Hoh|Hl|
		|Jesaja|Jes|
		|Jeremia|Jer|
		|Klagelied|Klagel|Klgl|Klag|Kla|Klg|
		|Hesekiel|Hes|Ez|
		|Daniel|Dan|Da|
		|Hosea|Hos|
		|Joel|Joe|
		|Amos|Am|
		|Obadja|Obad|Ob|
		|Jona|Jon|
		|Micha|Mich|Mi|
		|Nahum|Nah|
		|Habakuk|Hab|
		|Zephanja|Zefania|Zeph|Zef|Zep|
		|Haggai|Hag|Hg|
		|Sacharja|Sach|Sac|
		|Maleachi|Mal|
		|Matthäusevangelium|Matthäus|Matth|Mat|Mth|Mt|
		|Markusevangelium|Markus|Mark|Mk|
		|Lukasevangelium|Lukas|Luk|Lk|
		|Apostelgeschichte|Apg|Ag|Ap|
		|Römerbrief|Römer|Röm|Rm|Rö|
		|[1-2]\.?[\s ]?Korintherbrief|[1-2]\.?[\s ]?Korinther|[1-2]\.?[\s ]?Kor|[1-2]\.?[\s ]?Kr|
		|Galaterbrief|Galater|Gal|Ga|
		|Epheserbrief|Epheser|Eph|
		|Philemonbrief|Philemon|Phlm|Phm|
		|Philipperbrief|Philipper|Phil|Php|
		|Kolosserbrief|Kolosser|Kol|
		|[1-2]\.?[\s ]?Thessalonicherbrief|[1-2]\.?[\s ]?Thessalonicher|[1-2]\.?[\s ]?Thess|[1-2]\.?[\s ]?Thes|[1-2]\.?[\s ]?Th|
		|[1-2]\.?[\s ]?Timotheusbrief|[1-2]\.?[\s ]?Timotheus|[1-2]\.?[\s ]?Tim|[1-2]\.?[\s ]?Tm|
		|Titusbrief|Titus|Tit|Tt|
		|Hebräerbrief|Hebräer|Heber|Hebr|Heb|
		|Jakobusbrief|Jakobus|Jak|Jk|
		|[1-2]\.?[\s ]?Petrusbrief|[1-2]\.?[\s ]?Petrus|[1-2]\.?[\s ]?Petr|[1-2]\.?[\s ]?Pet|[1-2]\.?[\s ]?Ptr|[1-2]\.?[\s ]?Pe|[1-2]\.?[\s ]?Pt|[1-2]\.?[\s ]?P|
		|[1-3]\.?[\s ]?Johannesbrief|[1-3]\.?[\s ]?Johannes|[1-3]\.?[\s ]?Joh|[1-3]\.?[\s ]?Jh|[1-3]\.?[\s ]?Jo|
		|Johannesevangelium|Johannes|Joh|Jh|
		|Judasbrief|Judas|Jud|
		|Offenbarung|Offb|Off)' );

		// Einen einzigen Vers
		if ( ! defined( 'BibeleinzelstelleREGEXP' ) )
		{
			define( 'BibeleinzelstelleREGEXP', $Bibelbuecher . '[.,]?[\s ]+[1-9]\d{0,2}[b-d]?[\s ]?[,.:][\s ]?[1-9]\d{0,2}[a-d]?[\s ]?f?f?' );
		}
		// In einem Kapitel
		if ( ! defined( 'BibeltextREGEXP' ) )
		{
			define( 'BibeltextREGEXP', $Bibelbuecher . '[.,]?[\s ]+[1-9]\d{0,2}[b-d]?[\s ]?[,.:][\s ]?[1-9]\d{0,2}[a-d]?[\s ]?f?f?[\s ]?-[\s ]?\d{0,3}[a-d]?' );
		}
		// Über mehrere Kapitel
		if ( ! defined( 'BibelabschnittREGEXP' ) )
		{
			define( 'BibelabschnittREGEXP', $Bibelbuecher . '[.,]?[\s ]+[1-9]\d{0,2}[b-d]?[\s ]?[,.:][\s ]?[1-9]\d{0,2}[a-d]?[\s ]?f?f?[\s ]?-[\s ]?\d{0,3}[a-d]?[\s ]?[,.:][\s ]?\d{0,3}[a-d]?' );
		}
		// Ein oder mehrere Kapitel
		if ( ! defined( 'KapitelREGEXP' ) )
		{
			define( 'KapitelREGEXP', $Bibelbuecher . '[.,]?[\s ]+[1-9]\d{0,2}[\s ]?-?[\s ]?\d{0,3}' );
		}
		// Alle Arten Bibelstellen → Keine Kapitel
		if ( ! defined( 'BibelstelleREGEXP' ) )
		{
			define( 'BibelstelleREGEXP', $Bibelbuecher . '[.,]?[\s ]+[1-9]\d{0,2}[b-d]?[\s ]?[,.:][\s ]?[1-9]\d{0,2}[a-d]?[\s ]?f?f?[\s ]?-?[\s ]?\d{0,3}[a-d]?[\s ]?[,.:]?[\s ]?\d{0,3}[a-d]?' );
		}
		// Bibelbuchangabe
		if ( ! defined( 'BibelbuchREGEXP' ) )
		{
			define( 'BibelbuchREGEXP', $Bibelbuecher );
		}
		// Kapitel und oder Vers-Angabe
		if ( ! defined( 'KapitelVersREGEXP' ) )
		{
			define( 'KapitelVersREGEXP', '[\s ]?[1-9]\d{0,2}[b-d]?[\s ]?[,.:-]?[\s ]?\d{0,3}[a-d]?[\s ]?f?f?[\s ]?-?[\s ]?\d{0,3}[a-d]?[\s ]?[,.:]?[\s ]?\d{0,3}[a-d]?[\s ]?' );
		}

		// Links und Überschriften schützen
		if ( ! function_exists( 'SchutzmaskeKeineStelle' ) )
		{
			function SchutzmaskeKeineStelle( $a ) {
				return '{}-- ' . preg_replace( '/(\d)/', '&$1', base64_encode( $a[0] ) ) . ' --$$';
			}
		}
		// Bereits bearbeitete Stellenabfolge schützen
		if ( ! function_exists( 'SchutzVerlinkungStellenabfolge' ) )
		{
			function SchutzVerlinkungStellenabfolge( $b ) {
				return $b[1] . '{}-- ' . preg_replace( '/(\d)/', '&$1', base64_encode( "<a target=\"popup\" onclick=\"window.open('about:blank', 'popup', 'scrollbars=yes, toolbar=no,status=no,resizable=yes,menubar=no,location=no,directories=no,top=10,left=3000,width=600,height=800')\" href=\"https://www.gratia-mira.ch/j3/?Stichwort=" . urlencode( $b[2] ) . "&Plugin=Stellenabfolge\" title=\"Bibelstellen aufschlagen\" style=\"color:currentcolor;text-decoration:underline;\" target=\"_blank\" rel=\"noopener\">" . $b[2] . '</a>' ) ) . ' --$$' . $b[4];
			}
		}
		// Bereits bearbeitete Stellen schützen
		if ( ! function_exists( 'SchutzVerlinkungStellenabfolgeimText' ) )
		{
			function SchutzVerlinkungStellenabfolgeimText( $c ) {
				if ( preg_match( '/' . constant( 'BibelbuchREGEXP' ) . '/', $c[6] ) )
				{
					return $c[0];
				}
				else
				{

					$Link = "<a target=\"popup\" onclick=\"window.open('about:blank', 'popup', 'scrollbars=yes, toolbar=no,status=no,resizable=yes,menubar=no,location=no,directories=no,top=10,left=3000,width=600,height=800')\" href=\"https://www.gratia-mira.ch/j3/?Stichwort=" . urlencode( $c[0] ) . "&Plugin=Bibelstelle\" title=\"Bibelstelle aufschlagen\" style=\"color:currentcolor;text-decoration:underline;\" target=\"_blank\" rel=\"noopener\">" . $c[0] . '</a>';

					return $c[1] . '{}-- ' . preg_replace( '/(\d)/', '&$1', base64_encode( $Link ) ) . ' --$$';
				}
			}
		}
		// Bereits bearbeitete Stellen schützen
		if ( ! function_exists( 'SchutzVerlinkungStellen' ) )
		{
			function SchutzVerlinkungStellen( $c ) {
				$Link = "<a target=\"popup\" onclick=\"window.open('about:blank', 'popup', 'scrollbars=yes, toolbar=no,status=no,resizable=yes,menubar=no,location=no,directories=no,top=10,left=3000,width=600,height=800')\" href=\"https://www.gratia-mira.ch/j3/?Stichwort=" . urlencode( $c[2] ) . "&Plugin=Bibelstelle\" title=\"Bibelstelle aufschlagen\" style=\"color:currentcolor;text-decoration:underline;\" target=\"_blank\" rel=\"noopener\">" . $c[2] . '</a>';

				return $c[1] . '{}-- ' . preg_replace( '/(\d)/', '&$1', base64_encode( $Link ) ) . ' --$$';
			}
		}

		// Überschriften und oft falsch als Bibelstelle erkannte Muster schützen
		$SuchenLinksUeberschriften = array(
			'/<form.*\/form>/Us',
			'/<a.*\/a>/Us',
			// Formulardaten und Links
			'/(<h[1-9]>.*\/h[1-9]>)/Us',
			// Überschriften
			'/[0-2]?\d[:.][0-5]\d[\s ]?Uhr/',
			// 15:03 Uhr;
			'/\d{4,10}/',
			// 1000+
			'/(\b[^Ps0-9]{2}|[^\s (alm0-9]{3}|[^\s (men0-9]{3})[\s ]\d{3}/',
			// Lk 707 (alles dreistellige ausser Psalmen)
			'/Am[\s ]?\d{1,2}\.\d{1,2}\./',
			// Am 13.02.
			'/Am[\s ]?\d{1,2}\.[\s ]?(Jan|Feb|März|Apr|Mai|Jun|Jul|Aug|Sept|Okt|Nov|Dez)/',
			// Am 1. Juni
			'/Amo?s?[\s ]?([1-9]\d[.:,]?|\d{3})/',
			// Am 10+
			'/Oba?d?j?a?[\s ]?(1?,?2[2-9]|1?[.:,]?[3-9][0-9]|1?[.:,]?\d{3})/',
			// Ob 1,?22+
			'/Dan?i?e?l?[\s ]?(1[3-9][.:,]?|[2-9]\d,?|\d{3})/',
			// Da 13,?+
			'/\d{1,2}[\s ]+[A-ZÜÖÄ][a-zäöüß]{2,20}(([!?.;:]?[\s ]?|[\s ]+)[<]|([!?.;:]?[\s ]?|[\s ]+)$|[\s ]+[a-zäöüß])/m'
		); // 13 Esel</p> / 15 Kühe / 16 Menschen und 
		$Inhalt                    = preg_replace_callback( $SuchenLinksUeberschriften, 'SchutzmaskeKeineStelle', $Inhalt );

		// Bibelstellenabfolge in einen Link umwandeln + maskieren
		// Lange zu Kurze REGEXP Lk 2,1; 2,2; (500 ist die Obergrenze bei gratia-mira.ch)
		$SuchenBibelstellenabfolge = '/([^~])(\b' . $Bibelbuecher . constant( 'KapitelVersREGEXP' ) . '[\s ]{0,3}[\/.;][0-9-–?!\[\]\*+&\|\/=;.:,abcx  ]{2,500})([^0-9A-Za-zÄÖÜäöü]|[\s ][A-ZÄÖÜa-zäöü])/';

		// Wenn Bibelstellenabfolgen gleich nacheinander kommen, werden Stellen u.U. nur beim zweiten Mal erkannt
		$Inhalt = preg_replace_callback( $SuchenBibelstellenabfolge, 'SchutzVerlinkungStellenabfolge', $Inhalt );
		$Inhalt = preg_replace_callback( $SuchenBibelstellenabfolge, 'SchutzVerlinkungStellenabfolge', $Inhalt );

		// Verlinkung Stellenabfolgen mit Text (z.B. Stelle und Stelle ohne Buch sowie Stelle ohne Buch)
		$ZwischenworteArray = array(
			'und Vers',
			'und Kapitel',
			'und auch',
			'und',
			'&amp;',
			'&',
			'\+',
			'sowie auch',
			'sowie in',
			'sowie',
			'als auch',
			'sowie auch in Vers'
		);
		// Regex vorbereiten
		$ZwischenworteREGEXP = '[\s ]?(' . str_replace( ' ', '[\s ]?', implode( '|', $ZwischenworteArray ) ) . ')[\s ]?';
		$StelleRGEXP         = str_replace( ')', '|' . constant( 'KapitelVersREGEXP' ) . '|\d{1,3})', $Bibelbuecher );

		$SuchenBibelstellenabfolgeimText = array(
			'/([^~])(\b' . constant( 'BibelstelleREGEXP' ) . '|' . constant( 'KapitelREGEXP' ) . ')' . $ZwischenworteREGEXP . $StelleRGEXP . $ZwischenworteREGEXP . $StelleRGEXP . '/',
			'/([^~])(\b' . constant( 'BibelstelleREGEXP' ) . '|' . constant( 'KapitelREGEXP' ) . ')' . $ZwischenworteREGEXP . $StelleRGEXP . '/'
		);

		$Inhalt = preg_replace_callback( $SuchenBibelstellenabfolgeimText, 'SchutzVerlinkungStellenabfolgeimText', $Inhalt );


		// Bibelstelle und Kapitelangaben in einen Link umwandeln + maskieren
		$SuchenStelle = array(
			'/([^~])(' . constant( 'BibelabschnittREGEXP' ) . ')/',
			'/([^~])(' . constant( 'BibeltextREGEXP' ) . ')/',
			'/([^~])(' . constant( 'BibeleinzelstelleREGEXP' ) . ')/',
			'/([^~])(' . constant( 'KapitelREGEXP' ) . ')/'
		);
		$Inhalt       = preg_replace_callback( $SuchenStelle, 'SchutzVerlinkungStellen', $Inhalt );

		// Ersetzt am Ende alle ~, die dazu bestimmt sind, dass die Bibelstelle nicht verlinkt wird
		$Inhalt = preg_replace( '/\~(' . constant( 'KapitelREGEXP' ) . ')/', '$1', $Inhalt );

		// Jesus Christus und Gott in einen Link umwandeln;
		// nur im Joomlaplugin für gratia-mira.ch.
		if ( $context == 'mod_bibelversanzeige' )
		{
			$SuchenER = array( '/([^~])\b(Jesus Christus|Jesus|Christus)\b/', '/([^~])(Gott)([.,:! ])/' );
			$ErsetzenER =
				array(
					'$1<a href="https://www.gratia-mira.ch/j3/bibelverse/zu-jesus-christus" title="Zufälliger Bibelvers über Jesus Christus"  target="_blank" rel="noopener">$2</a>',
					'$1<a href="https://www.gratia-mira.ch/j3/bibelverse/ueber-gott" title="Zufälliger Bibelvers über Gott" target="_blank" rel="noopener">$2</a>$3'
				);
			$Inhalt = preg_replace( $SuchenER, $ErsetzenER, $Inhalt );
		}


		// Links und Überschriften wiederherstellen
		if ( ! function_exists( 'WiederherstellenMaske' ) )
		{
			function WiederherstellenMaske( $d ) {
				return base64_decode( str_replace( array( '{}-- ', ' --$$', '&' ), '', $d[0] ) );
			}
		}

		// Zurücksetzen bis alles gut ist :)
		do
		{
			$Inhalt = preg_replace_callback( '/\{\}\-\- .* \-\-\$\$/U', 'WiederherstellenMaske', $Inhalt );
		} while ( preg_match( '/\{\}\-\- .* \-\-\$\$/', $Inhalt ) == true );

		// Keine Leerzeichen und Sonderzeichen am Ende von Links.
		// Dies ist vorher nur schlecht zu vermeiden, da ja die grösstmögliche Bibelstelle erkannt werden soll.
		$Inhalt = preg_replace( '/([ ,.:;]| und )(<\/a>)/i', '$2$1', $Inhalt );

		// Je nach CMS unterschiedlich.
		// Im Rahmen des Plugins ein $article->text.
		// wird das Plugin in einem Modul eingesetzt ein return.
		$article->text = $Inhalt;

		return $Inhalt;

	}
}
